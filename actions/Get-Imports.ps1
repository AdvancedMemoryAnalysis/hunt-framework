﻿Function Get-Imports {
    param (
    [Parameter(mandatory=$false,Position=1)]
        [ValidateScript({Test-Path $_})]
        [string]$Path,
    [Parameter(mandatory=$false,Position=3)]
        [ValidateScript({Test-Path $_})]
        [string]$RaBin2='C:\Users\Kevin\AppData\Local\Programs\radare2\rabin2.exe'
    )

    $results = $null
    $results = & $RaBin2 -i -j $path
    $results = ($results | ConvertFrom-Json).Imports
    $results | %{$_ | Add-Member -NotePropertyName FileName -NotePropertyValue (Split-Path $path -Leaf)}
    $Global:Imports += $results
}
