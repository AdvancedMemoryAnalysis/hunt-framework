﻿Function Get-Exports {
    param (
    [Parameter(mandatory=$false,Position=1)]
        [ValidateScript({Test-Path $_})]
        [string]$Path='C:\Windows\System32',
    [Parameter(mandatory=$false,Position=2)]
        [ValidateScript({Test-Path $_})]
        [string]$OutPath="D:\Analysis\Output\Functions"
    )

    $dlls = dir $path -Filter *.dll | %{$_.FullName}
    $functionCollection = @()
    foreach ($dll in $dlls){
        echo "Getting Exports on: $($dll)"
        $outfile = $OutPath + "\" + (Split-Path -Leaf $dll) + ".csv"    
        $exportResults = & python .\getExports.py ($dll).Replace('\','/') 
        $functionCollection += $exportResults | ConvertFrom-Csv
    }

    $Global:Functions = $functionCollection

}
