﻿Function Invoke-Hunt-Differential-Analysis {
    param (
    [Parameter(mandatory=$false)]
        [string]$Hostname=$global:UI.Config.Session.Hostname
    )

    if($global:UI.config.knowngood) {

        $objCollection = @()
        foreach($entry in $global:UI.config.Differential_Analysis) {
            $diff = Get-DeepProperty -InputObject $global:UI.$($Hostname)     -Property $($entry.Path)
            $ref =  Get-DeepProperty -InputObject $global:UI.Config.Knowngood -Property $($entry.Path)

            $props = $entry.variables.split(',')

            $diffResults =@()
            $diffResults += Compare-Object -DifferenceObject $ref -ReferenceObject $diff -Property $props -PassThru | ?{$_.SideIndicator -eq '<='}

            if($entry.Name -eq 'doesntexist'){
                $iBreakHereOften=1
            }

            $objCollection += [pscustomobject]@{
                'Name'=$entry.Name
                'Count'=$diffResults.count
                'Results'=$diffResults
            }

        }

        $global:UI.$($hostname).DifferentialAnalysis =$objCollection
        return $objCollection

    }
    else {
        echo "Knowngood not loaded!"
    }

}