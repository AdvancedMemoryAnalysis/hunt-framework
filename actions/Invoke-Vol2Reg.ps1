﻿Function Invoke-VolGetReg {
    param (
        [Parameter(mandatory=$false,position=5)]
            [string]$Hostname=$Global:UI.Config.Session.Hostname,
        [Parameter(mandatory=$false,position=5)]
            [ValidateScript({Test-Path $_})]
            [array]$RegistryKeys=$Global:UI.Config.RegistryKeys,
        [Parameter(mandatory=$false,position=5)]
            [ValidateScript({Test-Path $_})]
            [string]$Volatility="$($Global:UI.Config.Session.ToolsPath)\volatility.exe",
        [Parameter(mandatory=$false,position=6)]
            [string]$Profile=$($Global:UI.Config.Session.MemoryProfile),
        [Parameter(mandatory=$false,position=7)]
            [ValidateScript({Test-Path $_})]
            [string]$MemoryImage=$Global:UI.Config.Session.MemoryFile
    )

    Begin{}
    Process{
        $Format = 'greptext'
        $registryCollection = @()
        foreach($key in $RegistryKeys){
            $memoryPathBlock = " -f $($MemoryImage)"
            $profileBlock = " --profile=$($Profile)"
            $fileTypeBlock = " --output=$($Format)"
            $pluginBlock = " printkey -K `'$($key.Path)`'" 

            $volcmd = $Volatility + $memoryPathBlock + $profileBlock + $fileTypeBlock + $pluginBlock

            $Error.Clear(); $ErrorActionPreference = "SilentlyContinue"; $ErrorView = "CatagoryView"
            $results = Invoke-Expression $volcmd
            $ErrorActionPreference = "Continue"; $ErrorView = "Normal"

            if($Error[0].Exception -match "Volatility Foundation Volatility Framework"){
                $Error.Clear()
            }
            else {
                Write-Error -Message $Error
            }
            
            $grepTextData=@()
            foreach($line in $results){
                $greptextData+=$line.replace('>|','')
            }

            $customObjectData = $grepTextData | ConvertFrom-Csv -Delimiter "|"
            
            #Volatility doesn't get me everything I want, so I am adding it here
            foreach($entry in $customObjectData) {
                $entry | Add-Member -NotePropertyName Root -NotePropertyValue $Key.Root
                $entry | Add-Member -NotePropertyName Hive -NotePropertyValue $Key.Hive
                $entry | Add-Member -NotePropertyName Path -NotePropertyValue $Key.Path
            }

            $registryCollection += $customObjectData
        }

        $Global:UI.$($Hostname).Memory += @{'Registry'=@()}
        $Global:UI.$($Hostname).Memory.Registry = $registryCollection

    }
    End{}
}