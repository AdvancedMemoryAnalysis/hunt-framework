﻿Function Invoke-Hunt-Vol-ProcDump {
    Param (
        [Parameter(mandatory=$false,position=1)]
            [string]$ProcID,
        [Parameter(mandatory=$false)]
            [string]$Name,
        [Parameter(mandatory=$false)]
            [string]$Profile=$($Global:UI.Config.Session.MemoryProfile),
        [Parameter(mandatory=$false)]
            [ValidateScript({Test-Path $_})]
            [string]$DumpDir="$($Global:UI.Config.Session.ExportPath)\DumpDir",
        [Parameter(mandatory=$false)]
            [ValidateScript({Test-Path $_})]
            [string]$MemoryImage="$($Global:UI.Config.Session.HuntFrameWorkMemPath)\$($Global:UI.Config.Session.Hostname).elf",
        [Parameter(mandatory=$false)]
            [ValidateScript({Test-Path $_})]
            [string]$Volatility="$($Global:UI.Config.Session.HuntFrameWorkPath)\tools\volatility.exe"
    )
    DynamicParam {
        
        if(!($ProcID -or $Name)){echo "`nYou must use PID or Name!`n"}
        if($ProcID -and $Name){echo "`nYou must use PID 'OR' Name (Not Both)!`n"}
    }
    Begin{
    }
    Process{
        
        if($ProcID){$volcmd = "$($Volatility) -f $($memoryImage) --profile=$($profile) procdump -p $ProcID -D $DumpDir"}
        if($Name){$volcmd = "$($Volatility) -f $($memoryImage) --profile=$($profile) procdump -n $Name -D $DumpDir"}
        
        $Error.Clear(); $ErrorActionPreference = "SilentlyContinue"; $ErrorView = "CatagoryView"
        Invoke-Expression $volcmd
        if($Error[0].Exception -match "Volatility Foundation Volatility Framework"){
            $Error.Clear()
        }
        else {
            Write-Error -Message $Error[0]
        }
    }
    End{}
}