﻿Function Import-Volatility {
        param (
        [Parameter(mandatory=$false)]
            [string]$Hostname=$Global:UI.Config.Session.Hostname,
        [Parameter(mandatory=$false)]
            [ValidateScript({Test-Path $_})]
            [string]$ImportPath=$Global:UI.Config.Session.ImportPath
        )


    begin{

        Get-Job | ?{$_.Name -match "vol-"} | Remove-Job

    }

    process {        
        $files = dir -Path $ImportPath -Filter *.greptext | %{$_.FullName}
        foreach($file in $files){
            $rawData = Get-Content -Path $file
            $csvData=@()
            foreach($line in $rawData){
                $csvData+=$line.replace('>|','')
            }
            $customObjectData = $csvData | ConvertFrom-Csv -Delimiter "|"
            $plugin = (Split-Path -Leaf $file).Split('.')[0] 
            $Global:UI.$($Hostname).Memory += @{$plugin=$customObjectData}
        }
    }
}