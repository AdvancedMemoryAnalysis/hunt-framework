﻿Function Vol-Scan {
        param (
        [Parameter(mandatory=$true,Position=1)]
            [ValidateScript({Test-Path $_})]
            [string]$MemoryPath,
        [Parameter(mandatory=$true,Position=2)]
            [ValidateScript({Test-Path $_})]
            [string]$OutPath,
        [Parameter(mandatory=$false,Position=3)]
            [string]$Profile="Win7SP1x86",
        [Parameter(mandatory=$false,Position=4)]
            [ValidateScript({Test-Path $_})]
            [string]$Volatility="$($huntFrameworkPath)\tools\volatility.exe",
        [Parameter(mandatory=$false,Position=5)]
            [ValidateScript({Test-Path $_})]
            [string]$PluginFilePath="$($huntFrameworkPath)\input\volatility\plugins.csv"
        )

    begin{
        
        if(!$UI.config.Vol_Plugins) {$UI.config.Plugins = Import-Csv $PluginFilePath}

    }
    process{
        
        foreach($plugin in $ui.config.Vol_Plugins) {    
            if($plugin.Run -eq $true) {

                $pluginBlock = $null 
                $volcmd = $null

                if($plugin.Options -ne $false) {$pluginBlock = " $($plugin.Name) $($options)" }
                if($plugin.Dump -eq $true) {
                    $dumpDir = "$($OutPath)\Dumpdir"
                    if(!(Test-Path $dumpDir)) {$r=mkdir $dumpDir}
                    $pluginBlock = " $($plugin.Name) -D $($dumpDir)"
                }
                else {
                    $pluginBlock = " $($plugin.Name)"
                }

                $memoryPathBlock = " -f $($MemoryPath)"
                $profileBlock = " --profile=$($Profile)"
                $fileTypeBlock = " --output=$($plugin.Format)"
                $outputBlock = " --output-file=$($OutPath)\$($plugin.Name).$($plugin.Format)"

                $volcmd = $Volatility + $memoryPathBlock + $profileBlock + $fileTypeBlock + $outputBlock + $pluginBlock

                $scriptBlock = {sleep $args[0]; $results = Invoke-Expression $args[1]}
                Start-Job -ScriptBlock $scriptBlock -ArgumentList $plugin.Time,$volcmd -Name "Vol-$($plugin.Name)"

            }
        }
    }
}