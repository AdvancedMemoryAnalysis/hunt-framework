﻿Function New-Hunt-Object {
    [CmdletBinding()]
    Param(
    )
    DynamicParam {
        
        
        # Set the dynamic parameters' name
        $ParamName_plugin = 'Name'
        # Create the collection of attributes
        $AttributeCollection = New-Object System.Collections.ObjectModel.Collection[System.Attribute]
        # Create and set the parameters' attributes
        $ParameterAttribute = New-Object System.Management.Automation.ParameterAttribute
        $ParameterAttribute.Mandatory = $true
        $ParameterAttribute.Position = 0
        # Add the attributes to the attributes collection
        $AttributeCollection.Add($ParameterAttribute) 
        # Create the dictionary 
        $RuntimeParameterDictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary
        # Generate and set the ValidateSet 
        $arrSet = $Global:UI.Config.Maps.getenumerator() | %{$_.Key}
        $ValidateSetAttribute = New-Object System.Management.Automation.ValidateSetAttribute($arrSet)    
        # Add the ValidateSet to the attributes collection
        $AttributeCollection.Add($ValidateSetAttribute)
        # Create and return the dynamic parameter
        $RuntimeParameter = New-Object System.Management.Automation.RuntimeDefinedParameter($ParamName_plugin, [string], $AttributeCollection)
        $RuntimeParameterDictionary.Add($ParamName_plugin, $RuntimeParameter)        
        return $RuntimeParameterDictionary


    }

    begin{
        $Name=$PSBoundParameters['Name']
    }
    process{
        $huntObject = New-Object -TypeName pscustomobject
        $Global:UI.Config.Maps.$($Name).getenumerator() | %{$huntObject | Add-Member -NotePropertyName $_.Key -NotePropertyValue $null}
        return $huntObject
    }
    end{}
}
