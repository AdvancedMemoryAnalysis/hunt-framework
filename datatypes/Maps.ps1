﻿$Global:UI.Config.Maps.Signature = @{
    'Signed'=$null
    'Signer'=$null
    'NotBefore'=$null
    'NotAfter'=$null
    'ThumbPrint'=$null
    'TimeStamper'=$null
    'TimeStamperThumbprint'=$null
}


$Global:UI.Config.Maps.Registry = @{
	'Hive'=$null
	'Root'=$null
	'Path'=$null
	'LastWrite'=$null
	'Value'=$null
	'Type'=$null
	'Data'=$null
}

$Global:UI.Config.Maps.Vol2Registry = @{
    'hData'=@('memory.registry')
    'hProperties'=$null
	'Hive'='Hive'
	'Root'='Root'
	'Path'='Path'
	'LastWrite'='LastWrite'
	'Value'='ValName'
	'Type'='ValType'
	'Data'='ValData'
}

$Global:UI.Config.Maps.Vol2Process = @{
    'hData'=@('memory.pslist','memory.cmdline')
    'hProperties'=@('pid','pid')
	'Name'='Process'
	'Path'=$null
	'PID'='PID'
	'PPID'='PPID'
	'Parent'=$null
	'Arguments'='CommandLine'
    'Handles'='Hnds'
    'Threads'='Thds'
    'Session'='Sess'
}

$Global:UI.Config.Maps.Vol2DLLs = @{
    'hData'=@('memory.ldrmodules')
    'hProperties'=$null
    'Process'='Process'
    'DLL'=$null
    'Path'='MappedPath'
    'PID'='Pid'
}