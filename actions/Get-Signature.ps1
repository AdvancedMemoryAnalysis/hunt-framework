﻿Function Get-Signature {
    [CmdletBinding()]

    Param(
        [Parameter(
            Mandatory=$True,
            ValueFromPipeline=$True,
            ValueFromPipelinebyPropertyName=$True
        )]
      [object]$Object
    )
    begin{}
    process{
    
        $signData = Get-AuthenticodeSignature -FilePath $Object.path
        $signObj = New-Hunt-Object -Name Signature
        $signObj.Signed=$signData.Status
        $signObj.Signer=$signData.SignerCertificate.DnsNameList
        $signObj.NotBefore=$signData.SignerCertificate.NotBefore
        $signObj.NotAfter=$signData.SignerCertificate.NotAfter
        $signObj.ThumbPrint=$signData.SignerCertificate.Thumbprint
        $signObj.TimeStamper=$signData.TimeStamperCertificate.DnsNameList
        $signObj.TimeStamperThumbprint=$signData.TimeStamperCertificate.Thumbprint

        return $signObj
    
    }
    end{}
}