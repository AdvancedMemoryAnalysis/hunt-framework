﻿Function Invoke-Volatility-Cmd {
    param (
    [Parameter(mandatory=$true)]
        [string]$VolCMD
    )

    $Error.Clear(); $ErrorActionPreference = "SilentlyContinue"; $ErrorView = "CatagoryView"
    Invoke-Expression $volcmd
    if($Error[0].Exception -match "Volatility Foundation Volatility Framework"){
        $Error.Clear()
    }
    else {
        Write-Error -Message $Error[0]
    }
}