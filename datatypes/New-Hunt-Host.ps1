﻿Function New-Hunt-Host {
    $huntHost = [psobject]@{
        'Processes'=@()
        'Drivers'=@()
        'DLLs'=@()
        'Files'=@()
        'Registry'=@()
        'WindowsEventLogs'=@()
        'Memory'=@{}
        'DifferentialAnalysis'=@()
        'Hunt'=@()
    }

    return $huntHost
}