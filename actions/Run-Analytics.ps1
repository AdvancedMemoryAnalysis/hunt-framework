﻿Function Invoke-Hunt-Signatures {
    [CmdletBinding()]
        param (
        [Parameter(mandatory=$false)]
            [string]$Hostname=$Global:UI.Config.Session.Hostname
        )

    if($Global:UI.$($Hostname)) {
        $huntSignatureCollection = @()
        foreach($signature in $Global:UI.Config.HuntSignatures) {
                    
            $data = Get-DeepProperty -InputObject $global:UI.$($Hostname) -Property $($signature.Source)

            if($data) {
                if($signature.Filter) {
                $filters = $signature.Filter.Split(";")
                for($i=0 ; $i -lt $filters.count ; $i++) {

                    $filter = $filters[$i].Split(':')

                    switch($filter[1]) {
                        "EQUAL"{$data = foreach($entry in $data){if($entry.$($filter[0]) -eq $filter[2]){$entry}}}
                        "NOT_EQUAL"{$data = foreach($entry in $data){if($entry.$($filter[0]) -ne $filter[2]){$entry}}}
                        "IN_MATCHES"{$data = foreach($entry in $data){if($entry.$($filter[0]) -match $filter[2]){$entry}}}
                        "NOT_IN_MATCHES"{$data = foreach($entry in $data){if($entry.$($filter[0]) -notmatch $filter[2]){$entry}}}
                        "GREATER_THAN"{$data = foreach($entry in $data){if($entry.$($filter[0]) -gt $filter[2]){$entry}}}
                        "LESS_THAN"{$data = foreach($entry in $data){if($entry.$($filter[0]) -lt $filter[2]){$entry}}}
                        "IS_NOT_NULL"{$data = foreach($entry in $data){if($entry.$($filter[0]) -ne $null){$entry}}}
                    }
                }
            }   
                
                $results = @()       
                if($data) {
                    if($data.count -gt 0) {
                        $count=0
                        foreach($entry in $data) {
                            $results += $entry
                            $count++
                        }
                    }
                    else {
                        $count = 1
                        $results += $data
                    }
                    $ran=$true
                }
                else {
                    $count=0
                    $ran=$false
                }
            }

            $huntSignatureCollection += [pscustomobject]@{
                'Name'=$signature.Name
                'Count'=$count
                'Ran'=$ran
                'Results'=$results
            }
        }
        $Global:UI.$($Hostname).Hunt = $huntSignatureCollection
    }
    else {echo "No data for that host?"}
}