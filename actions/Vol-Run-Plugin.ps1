﻿Function Invoke-Volatility-Plugin {
        param (
        [Parameter(mandatory=$false,position=1)]
            [ValidateSet('stdout','dot', 'greptext', 'html', 'json', 'sqlite', 'text', 'xlsx')]
            [string]$Format='stdout',
        [Parameter(mandatory=$false,position=2)]
            [string]$Options,
        [Parameter(mandatory=$false,position=3)]
            [switch]$Dump,
        [Parameter(mandatory=$false,position=4)]
            [string]$ExportPath=$Global:UI.Config.Session.ExportPath,
        [Parameter(mandatory=$false,position=5)]
            [ValidateScript({Test-Path $_})]
            [string]$Volatility="$($Global:UI.Config.Session.Toolspath)\volatility.exe",
        [Parameter(mandatory=$false,position=6)]
            [string]$Profile=$($Global:UI.Config.Session.MemoryProfile),
        [Parameter(mandatory=$false,position=7)]
            [ValidateScript({Test-Path $_})]
            [string]$MemoryImage= $Global:UI.Config.Session.MemoryFile
        )

    DynamicParam {
        
        
        # Set the dynamic parameters' name
        $ParamName_plugin = 'Plugin'
        # Create the collection of attributes
        $AttributeCollection = New-Object System.Collections.ObjectModel.Collection[System.Attribute]
        # Create and set the parameters' attributes
        $ParameterAttribute = New-Object System.Management.Automation.ParameterAttribute
        $ParameterAttribute.Mandatory = $true
        $ParameterAttribute.Position = 0
        # Add the attributes to the attributes collection
        $AttributeCollection.Add($ParameterAttribute) 
        # Create the dictionary 
        $RuntimeParameterDictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary
        # Generate and set the ValidateSet 
        $arrSet = ($Global:UI.config.vol_plugins).Name
        $ValidateSetAttribute = New-Object System.Management.Automation.ValidateSetAttribute($arrSet)    
        # Add the ValidateSet to the attributes collection
        $AttributeCollection.Add($ValidateSetAttribute)
        # Create and return the dynamic parameter
        $RuntimeParameter = New-Object System.Management.Automation.RuntimeDefinedParameter($ParamName_plugin, [string], $AttributeCollection)
        $RuntimeParameterDictionary.Add($ParamName_plugin, $RuntimeParameter)        
        return $RuntimeParameterDictionary


    }

    begin{
    
        $plugin = $PsBoundParameters[$ParamName_plugin]

    }
    process{
        

        $ExportFile = "$($ExportPath)\$($plugin).$($Format)"

        $pluginBlock = $null 
        $volcmd = $null

        if($Options) {
            $pluginBlock = " $($plugin) $($options)" 
        }
        else {
            $pluginBlock = " $($plugin)"
        }

        if($Dump) {
            $dumpDir = "$($ExportPath)\Dumpdir"
            if(!(Test-Path $dumpDir)) {$r=mkdir $dumpDir}
            $pluginBlock = " $($pluginBlock) -D $($dumpDir)"
        }

        $memoryPathBlock = " -f $($MemoryImage)"
        $profileBlock = " --profile=$($Profile)"
        if($Format -ne 'stdout') {
            $fileTypeBlock = " --output=$($Format)"
            $outputBlock = " --output-file=$($ExportPath)\$($plugin).$($Format)"
        }

        $volcmd = $Volatility + $memoryPathBlock + $profileBlock + $fileTypeBlock + $outputBlock + $pluginBlock
        #$volcmd | clip

        $Error.Clear(); $ErrorActionPreference = "SilentlyContinue"; $ErrorView = "CatagoryView"
        Invoke-Expression $volcmd
        
        if($Error[0].Exception -match "Volatility Foundation Volatility Framework"){
            $Error.Clear()
        }
        else {
            Write-Error -Message $Error
        }

        if(Test-Path $ExportFile) {
            & $Global:UI.Config.Session.Editor $ExportFile
        }
    }
}