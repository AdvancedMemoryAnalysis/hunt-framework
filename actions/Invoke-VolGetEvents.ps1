﻿Function Invoke-VolGetEvents {
    param (
        [Parameter(mandatory=$false,position=1)]
            [string]$Hostname=$Global:UI.Config.Session.Hostname,
        [Parameter(mandatory=$false,position=2)]
            [ValidateScript({Test-Path $_})]
            [string]$ExportPath=$Global:UI.Config.Session.ExportPath,
        [Parameter(mandatory=$false,position=3)]
            [string]$Profile=$($Global:UI.Config.Session.MemoryProfile),
        [Parameter(mandatory=$false,position=4)]
            [ValidateScript({Test-Path $_})]
            [string]$MemoryImage=$Global:UI.Config.Session.MemoryFile,
        [Parameter(mandatory=$false,position=5)]
            [ValidateScript({Test-Path $_})]
            [string]$Volatility="$($Global:UI.Config.Session.ToolsPath)\volatility.exe"
    )

    Begin{}
    Process{
        
        $eventPath = "$($ExportPath)\EventLogs"
        $dumpPath = "$($ExportPath)\DumpDir"

        if($UI.$($Hostname).Memory.dlllist) {
            $dlllist = $UI.$($Hostname).Memory.dlllist
        }
        else {
            echo "Run DllList!"
        }
        
        $wevtsvc = $dlllist | ?{$_.Path -match "wevtsvc"}                                       

        Invoke-Volatility-Plugin -Plugin dumpfiles -Format stdout -Dump -Options " -n -p $($wevtsvc.Pid)"                         
                                                                                  
        if(!(Test-Path $eventPath)){$mkdirRestults = mkdir $eventPath}                                                     
        
        dir $dumpPath -Filter *evtx* | %{mv $_.FullName $eventPath}

        $eventCollection = @{}
        $eventFiles = dir $eventPath
        foreach($eventFile in $eventFiles) {
            if($eventFile.Length -eq 262144 -or $eventFile.Length -eq 69632){
                rm $eventFile.fullname
                #echo "Remove File: $($eventFile.Name)"
            }            
            else {
                $fileName = $eventFile.FullName.Replace('.vacb','')    
                $fileName = $fileName.Replace('Microsoft-Windows-','')
                $fileName = $fileName.Replace('microsoft-windows-','')
                $fileName = $fileName.Replace('.dat','')
                $fileName = $fileName.Replace('%4','_')
                $fileName = $fileName.Replace(' ','_')
                $fileName = $fileName.Split('\')[-1].split('.')[3] + ".evtx"            
                mv $eventFile.FullName "$($eventPath)\$($fileName)"
                echo "Move $($eventFile.Name) to: $($fileName)"
            }
        }

        $ErrorActionPreference = "SilentlyContinue"; $ErrorView = "CatagoryView"
        $eventFiles = dir $eventPath
        foreach($eventFile in $eventFiles) {
            $eventCollection += @{$eventFile.Name.split('.')[0] = (Get-WinEvent -Path $eventFile.fullname)}
        }

        $ui.$($Hostname).WindowsEventLogs = $eventCollection

    }

    End {}

}